import threading
import Queue
import sys

class QueueWorker(threading.Thread):
	
	def __init__(self,queue):
		self.queue = queue
		self.isRunning = False
		self.timeout = 10
		self.init()
		threading.Thread.__init__(self)
	
		
	def init(self):
		pass # override-me
		
	def parse(self,item):
		pass # override-me
	
	def end(self):
		pass # override-me
		
	def stop(self):
		self.isRunning = False
	
	def run(self):
		self.isRunning = True
		while self.isRunning:
			try:
				item = self.queue.get(True,20)
				self.parse(item)
				self.queue.task_done()
			except Queue.Empty:
				self.isRunning = False
				
		self.end()
			
		
			
		
		

class Jobs:
	def __init__(self):
		self.jobs = []

	def start(self):
		for j in self.jobs:
			j.start()

	def stop(self):
		for j in self.jobs:
			j.stop()

	def add(self,obj):
		self.jobs.append(obj)

	def wait(self):
		try:
			for j in self.jobs:
				while j.isAlive():
					j.join(8)


		except KeyboardInterrupt:
			for j in self.jobs:
				j.stop()
			print "interrupted."
			sys.exit(1)
