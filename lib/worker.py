#!/usr/bin/env python2
# -*- coding: utf-8 -*-


import re
import sys
import jobs
import httplib2
from url import URL
from html import HTML
from color import color


'''
	@sha0coder

	TODO: gestionar timeouts
'''

global qcrawled

class HTTP:
	def __init__(self):
		self.isVerbose = True
		self.http = httplib2.Http(disable_ssl_certificate_validation=True)
		self.page404 = -1
		self.method = 'HEAD'
		self.headers = {
			"Content-type": "application/x-www-form-urlencoded",
			"Accept": "text/plain",
			"User-agent": "Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/20100101 Firefox/20.0",
			"Connection": "keepalive"
		}
	
	def __print(self,msg):
		if self.isVerbose:
			sys.stdout.write('verbose: %s%s\r' % (color['clear'],msg))
			sys.stdout.flush()

	def setCookie(self,cookie):
		self.headers.update({'Cookie':cookie})

	def setHost(self,host):
		self.headers.update({'Host':host})
			
	def send(self,path,method=None):
		self.__print(path)

		'''
			Who call this method can fix a mandatory method get or head,
			if no method is provided, then the method will ve readed from settings file, 
			unless a custom404 is fixed, in this case will be GET
		'''

		if not method:
			method = self.method
			if method == 'HEAD' and self.page404 != -1:
				method = 'GET'
		try:
			resp,html = self.http.request(path,method,headers=self.headers)
			return resp,html
		except:
			return {},''



class DirWorker(jobs.QueueWorker):
	
	def init(self):
		self.wl = []
		self.page404 = -1
		self.trace = False
		self.http = HTTP()


	def setScannedQueue(self,qscanned):
		self.qscanned = qscanned
		
	def setSettings(self,settings):
		self.settings = settings
		self.http.save = settings.save
		self.http.isVerbose = settings.isVerbose
		if self.settings.cookies:
			self.http.setCookie(self.setting.cookies)
		self.http.method = settings.method
		
	def setUrl(self,oUrl):
		self.oUrl = oUrl
		self.http.setHost(oUrl.getHost())
		
	def setWordlist(self,wl):
		self.wl = wl
		
	def setCustom404(self,custom404):
		self.page404 = custom404
		self.http.page404 = custom404
	
	def end(self):
		pass
		#print "worker end"
		
	def push(self,ddir):           # is isDirListing no entrar aqui!
		#print "pusing %s" % dir
		
		if 'data:image' in ddir:
			return

		self.qscanned.append(ddir)
		resp,html = self.http.send(ddir,'GET')
		if HTML(html,ddir).isDirlisting:
			return
		
		if '.' in ddir.split('/')[-1]:
			return
		
		for w in self.wl:
			for e in self.settings.ext:
				u=ddir+w+e
				if u not in self.qscanned:
					self.qscanned.append(u)
					self.queue.put(u)
		

		
	def _log(self,msg):
		if self.settings.save:
			open(self.settings.save,"a+").write(msg+"\n")

	
	def parse(self,path):
		if path.endswith('//'): # ???
			return
		
		path = re.sub('^:/+','/',path)
		resp,html = self.http.send(path)
		self.classify(path, resp, html)
		
		
	def _crawl(self,url,isDirlisting):
		if self.trace:
			print "_crawl(%s,%s)" % (url.getUrl(),isDirlisting)

		for u in url.getSubUrls():
			newurl = u.getPath()
			if "data:image" in newurl:
				continue
			
			# comprobar que realmente exite el directorio
			#print "sub urls %s " % newurl
			
			resp,html = self.http.send(self.oUrl.getIPbaseDirectory()+newurl,'GET')
			self.classify(self.oUrl.getIPbaseDirectory()+newurl, resp, html)			#TODO: si se llama mucho a getIPbaseDirectory() cachear
			if not resp:
				continue
			if int(resp.status) == 404:
				continue
			
			if u.isDirectory():
				if HTML(html,u).isDirlisting:
					# no escanear un directorio con dirlisting
					#self.done.push(newurl)
					
					if self.trace:
						print "is dirlisting "+newurl
					self.push(self.urlbase+newurl)
				else:
					if self.trace:
						print "is dir "+newurl
					self.push(self.urlbase+newurl)
					#self.AllpushDir(newurl)
			else:
				if self.trace:
					print "is file "+newurl
				self.push(self.urlbase+newurl)
			

		
		
	def crawl(self,path,html,isDirlisting):
		if not self.settings.doCrawl:
			return
		
		if self.trace:
			print "crawl(%s,%s)" % (path,isDirlisting)
		
		orig = URL(path)
		html = HTML(html,orig)
		
		for ref in html.getUrls():
			for su in ref.getSubUrls():
				if 'data:image' in su.getIPurl():
					continue
					
				if su.isDirectory():
					self.push(su.getIPurl()) # if is dirlisting don't push!!
				
				else:
					ipurl = su.getIPurl()
					if ipurl not in self.qscanned:
						self.qscanned.append(ipurl)
						self.queue.put(ipurl)
					
			
			'''
			# Hidden termiation check
			show = True
			for h in self.settings.hide:
				if ref.endswith(h):
					show = False
					break
				
			if not show:
				continue
			
			if ref.path == '/':
				continue

			self._crawl(ref,isDirlisting)
			'''


	def ip2name(self,path):
		spl = path.split('/')
		spl[2] = self.oUrl.getHostPort()
		return '/'.join(spl)
	
	
	def classify(self,path,resp,html):
		if self.trace:
			print "parse(%s)" % path

		if not resp:
			#print '%s%s[timeout!!]\t\t%s%s%s' % (color['clear'],color['red'][0], base,path, color['clean']);
			return

		code = int(resp.status)
		base = self.oUrl.getIPbase();
		
		if self.page404 >= 0:
			if self.page404 == len(html.split(' ')):
				code = 404

		if code == 200:
			hpath = self.ip2name(path)
			
			if self.page404 == -1:
				resp,html = self.http.send(path,'GET')

			orig = URL(path)

			if HTML(html,orig).isDirlisting():
				self._log('[200 Ok DirListing]\t%s' % hpath)
				print('%s%s[200 Ok DirListing]\t%s%s' % (color['clear'], color['green'][0], hpath, color['clean']))
				self.crawl(path,html,True)

			else:
				self._log('[200 Ok] (%d bytes)\t%s' % (len(html),hpath))
				print '%s%s[200 Ok] (%d bytes)\t%s%s' % (color['clear'],color['green'][0],len(html),hpath,color['clean'])
				if path.endswith('/'):
					self.push(path) # y si no acaba en / ?
				else:
					self.crawl(path,html,False)


		elif code >= 301 and code <= 303:
			hpath = self.ip2name(path)
			location = ''
			if resp.has_key('location'):
				if resp['location'].startswith('http'):
					location = URL(resp['location'])
				else:
					location = URL(self.oUrl.getBase()+resp['location'])
				
			if location and not self.settings.onlyshow200:
				self._log('[%d Redirect]\t\t%s\t-->\t%s' % (code,hpath,location.getUrl()))
				print '%s%s[%d Redirect]\t\t%s\t-->\t%s%s' % (color['clear'],color['cyan'][0],code,hpath,location.getUrl(),color['clean'])
				if location.getHost() == self.oUrl.getHost() or location.getIP() == self.oUrl.getIP():
					u = location.getIPurl()
					if u not in self.qscanned:
						self.qscanned.append(u)
						self.queue.put(u)
					
 
		elif code == 401 and not self.settings.onlyshow200:
			hpath = self.ip2name(path)
			self._log('[%s Auth needed]\t%s' % (code,hpath))
			print '%s%s[%s Auth needed]\t%s%s' % (color['clear'],color['magenta'][0],code,hpath,color['clean']);
		
		elif code == 403 and not self.settings.onlyshow200:
			hpath = self.ip2name(path)
			resp, html = self.http.send(path+'/asdfasdfasdf','GET') #TODO: randomize usar __send()
			if resp:
				if int(resp.status) == 404:
					self._log('[%s DirList denied]\t%s' % (code,hpath))
					print '%s%s[%s DirList denied]\t%s%s' % (color['clear'],color['yellow'][0],code,hpath,color['clean']);
					self.push(path) # seguro que es un directorio?
				else:
					self._log('[%s Permission Denied]\t%s' % (code,hpath))
					print '%s%s[%s Permission Denied]\t%s%s' % (color['clear'],color['yellow'][0],code,hpath,color['clean']);
	
		elif code == 404:
			pass
		
		elif not self.settings.onlyshow200:
			hpath = self.ip2name(path)
			self._log('[%s] \t\t\t%s' % (code,hpath))
			print '%s%s[%s] \t\t\t%s%s' % (color['clear'],color['blue'][0],code,hpath,color['clean']);

