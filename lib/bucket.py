# -*- coding: utf-8 -*-
import sys

'''
	Bucket container 
	@sha0coder
'''


class Bucket():
	def __init__(self):
		self.data = []

	def __getitem__(self,idx):
		return self.data[idx]

	def __setitem__(self,idx,content):
		self.data[idx] = content

	def __len__(self):
		return len(self.data)

	def __iter__(self):
		for d in self.data:
			yield d

	def __str__(self):
		ret = '['
		for d in self.data:
			ret += d+','
		return ret[:-1]+']'


	def load(self,filename):	
		try:
			fd = open(filename,'r')
			self.data = fd.readlines()
			fd.close()
			for i in range(0,len(self.data)):
				self.data[i] = self.data[i].replace('\n','').replace('\r','')
			print "loaded %d words" % len(self.data)
		except:
			print 'cant open file '+filename
			sys.exit()
	

	def pop(self):
		return self.data.pop()

	def push(self,item):
		self.data.insert(0,item)

	def clear(self):
		self.data = []

	def sz(self):
		return len(self.data)

	def exists(self,item):
		for i in self.data:
			if i == item:
				return True
		return False

	def uniq(self):
		self.data.sort()
		data=[]
		last=''
		for n in self.data:
			if n != last:
				data.append(n)
			last = n
		self.data = data
