import sys
from config import ConfigParser


class Settings:
	def __init__(self,filename):
		try:
			config = ConfigParser(filename).getConfig()
			self.timeout = int(config['timeout'])
			self.threads = int(config['threads'])
			self.cookies = config['cookies']
			self.wordlist = config['wordlist']
			self.ext = ['/']
			if config['extensions'] != '':
				self.ext += config['extensions'].split(',')
			self.isVerbose = (config['verbose'] == 'yes')
			if config['hide'] == '':
				self.hide = []
			else:
				self.hide = config['hide'].split(',')
			self.doCrawl = (config['crawl'] == 'yes')
			if config['noscan'] == '':
				self.noscan = []
			else:
				self.noscan = config['noscan'].split(',')
			self.save = config['save']
			self.saveFD = None
			self.onlyshow200 = (config['onlyshow200'] == 'yes')
			self.method = config['method']

		except:
			print('Bad config file.')
			sys.exit(1)

