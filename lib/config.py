'''
	@sha0coder
'''
import re 

class ConfigParser:
	def __init__(self,filename):
		self.comment = re.compile('^#')
		self.data = {}
		self.filename = filename
		self.load()

	def load(self):
		fd = open(self.filename,'r')
		for d in fd.readlines():
			d = d.replace('\t',' ').replace(' ','').replace('\n','').replace('\r','')
			if not self.comment.match(d):
				if d.find(':') >= 0:
					k,v = d.split(':')
					self.data[k]=v
		fd.close()

	def getConfig(self):
		return self.data

