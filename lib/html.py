
'''
	HTML simple parser
	@sha0coder
'''

import re
from url import URL
from bucket import Bucket


class HTML:
	def __init__(self,html,url):
		self.url = url # URL() object source of this html code
		self.html = html
		self.sz = len(html)
		self.href = re.compile('(href|src) *= *["\']([^"\']+)["\']')

	def isDirlisting(self):
		return (self.html.find('<title>Index of /') >= 0)

	def getUrls(self):
		'''
			Read all hrefs and return in a array with the format /aa/bb/cc.php
		'''
		
		urls = Bucket()

		for u in self.href.findall(self.html):
			url = u[1]
			if url.startswith('?') or url.startswith('mailto:') or url == '/':
				continue


			if url.startswith('http'):  #URL href="http://xxx.com/lala"  TODO: usar pathparts here
				u = URL(url)
				if u.getHost() != self.url.getHost():
					continue

				#print "** http %s" % u.getUrl()
				urls.push(u)

			
			elif url.startswith('/'):   #ABSOLUTE PATH  href="/lala/lalo"
				urls.push(URL(self.url.getBase()+url))

				#print "** /  %s %s" % (self.url.getBase(),url)

			else:

				#print "** no /   %s %s" % (self.url.getDirectory(),url)
				urls.push(URL(self.url.getBase()+self.url.getDirectory()+url))
		
		urls.uniq()
		return urls


		'''
			si la url encontrada es /lala/lalo se pushea talcual
			pero si la url es lala/lalo hay que concatenar el directorio base
		'''