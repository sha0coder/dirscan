#!/usr/bin/env python2
# -*- coding: utf-8 -*-

'''
	@sha0coder
	multi threaded & full featured  web directory scanner & crawler
	
	v1.1
'''

import sys
from lib.url import URL
from lib.bucket import Bucket
from lib.settings import Settings
import httplib2
import Queue
import lib.jobs
import lib.worker



class DirScan:

	def __init__(self,url,configfile):
		self.settings = Settings(configfile)
		self.oUrl = URL(url)
		self.wl = Bucket()
		self.wl.load(self.settings.wordlist)
		self.workers = []
		

	def __del__(self):
		pass # deallocate wordlist queues and jobs

	def setOutput(self,outfile):
		self.settings.save = outfile # override the settings file to store with the param provided

	def start(self):
		p404 = self.test404(self.oUrl.getIPurl())
		
		queue = Queue.Queue()
		qscanned = []
		jobmgr = lib.jobs.Jobs()
		
		for i in range(self.settings.threads):
			w = lib.worker.DirWorker(queue)
			w.setScannedQueue(qscanned)
			w.setCustom404(p404)
			w.setUrl(self.oUrl)
			w.setWordlist(self.wl)
			w.setSettings(self.settings)
			jobmgr.add(w)
			
		for w in self.wl:
			for e in self.settings.ext:
				queue.put(self.oUrl.getIPurl()+w+e)
			
		jobmgr.start()
		try:
			jobmgr.wait()
			queue.join(); # non blocking
		except KeyboardInterrupt:
			print "interrupted."
			
		
		#jobmgr.wait() 
	
			
	def test404(self,url):
		try:
			http = lib.worker.HTTP()
			resp,html = http.send(url+'/k23fj4fj0af','GET')
			if int(resp.status) != 404:
				print "Custom 404 page detected!"
				return len(html.split(' '))

		except Exception, e:
			print "site down\n "+str(e)
			sys.exit(1)
	
		return -1
			

	def end(self):
		for w in self.workers:
			w.end()
		if self.settings.saveFD: 
			self.settings.saveFD.close()


if __name__ == '__main__':
	if len(sys.argv) == 2 or len(sys.argv) == 3:
		url = sys.argv[1]  #TODO: check first if is a directory
		if url[-1] != '/':
			url += '/'
		ds = DirScan(url,'dirscan.conf')
		if len(sys.argv) == 3:
			ds.setOutput(sys.argv[2])
		ds.start()

	else:
		print "USAGE: %s [url] <optional results file>" % sys.argv[0]
	
